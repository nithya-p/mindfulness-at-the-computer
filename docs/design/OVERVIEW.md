
# Design Overview

## Structure

Design documentation:
* [Theory and background](theory-and-background.md)
* [User perspective](user-perspective.md)
* [Flowcharts and Wireframes](flowcharts-and-wireframes.xml)

Other links:
* [Gitter room: Design](https://gitter.im/mindfulness-at-the-computer/design)
* [Feature ideas (wiki)](https://gitlab.com/mindfulness-at-the-computer/mindfulness-at-the-computer/wikis/Feature-Ideas)
* [Open issues with the design tag](https://gitlab.com/mindfulness-at-the-computer/mindfulness-at-the-computer/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=design)

## Process

To make changes to the application features please follow this procedure:
* Discuss with the others in the Gitter design room
* Create issue with the design tag
  * Perhaps also the question tag if it is unsure if this is going to be implemented
* Update the design documentation
* Implement

