<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="vi" sourcelanguage="en">
  <context>
    <name>BreathingDialogComing</name>
    <message>
      <location filename="../../mc/gui/intro_dlg.py" line="327"/>
      <source>Finish</source>
      <translation type="unfinished"/>
    </message>
  </context>
  <context>
    <name>BreathingNotification</name>
    <message>
      <location filename="../../mc/gui/breathing_notification.py" line="62"/>
      <source>Please slow down and prepare for your breathing break. Please adjust your posture</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/breathing_notification.py" line="79"/>
      <source>Close</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/breathing_notification.py" line="85"/>
      <source>Show Dialog</source>
      <translation type="unfinished"/>
    </message>
  </context>
  <context>
    <name>BreathingPhraseListWt</name>
    <message>
      <location filename="../../mc/gui/breathing_phrase_list_wt.py" line="31"/>
      <source>New item</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/breathing_phrase_list_wt.py" line="40"/>
      <source>Add</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/breathing_phrase_list_wt.py" line="49"/>
      <source>Edit the selected breathing phrase</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/breathing_phrase_list_wt.py" line="55"/>
      <source>Move the selected breathing phrase to top</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/breathing_phrase_list_wt.py" line="61"/>
      <source>Move the selected breathing phrase up</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/breathing_phrase_list_wt.py" line="67"/>
      <source>Move the selected breathing phrase down</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/breathing_phrase_list_wt.py" line="73"/>
      <source>Delete the selected breathing phrase</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/breathing_phrase_list_wt.py" line="85"/>
      <source>These are the sentences that appear in the `breathing dialog`</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/breathing_phrase_list_wt.py" line="209"/>
      <source>Cannot remove entry</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/breathing_phrase_list_wt.py" line="209"/>
      <source>You need to have at least one entry in the breathing list</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/breathing_phrase_list_wt.py" line="216"/>
      <source>Are you sure that you want to remove this entry?</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/breathing_phrase_list_wt.py" line="232"/>
      <source>You have to write an item before you press &apos;Add&apos;.</source>
      <translation type="unfinished"/>
    </message>
  </context>
  <context>
    <name>BreathingSettingsWt</name>
    <message>
      <location filename="../../mc/gui/breathing_settings_wt.py" line="22"/>
      <source>Visual + Audio</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/breathing_settings_wt.py" line="23"/>
      <source>Visual</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/breathing_settings_wt.py" line="24"/>
      <source>Audio</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/breathing_settings_wt.py" line="25"/>
      <source>Same</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/breathing_settings_wt.py" line="26"/>
      <source>Random</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/breathing_settings_wt.py" line="27"/>
      <source>Select audio</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/breathing_settings_wt.py" line="67"/>
      <source>Dialog</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/breathing_settings_wt.py" line="93"/>
      <source>Notifications</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/breathing_settings_wt.py" line="95"/>
      <source>Notification type</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/breathing_settings_wt.py" line="100"/>
      <source>Do you always want the same phrase or a random one?</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/breathing_settings_wt.py" line="102"/>
      <source>Dialog Audio</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/breathing_settings_wt.py" line="109"/>
      <source>Settings for Breathing</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/breathing_settings_wt.py" line="198"/>
      <source>Please choose a wav audio file</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/breathing_settings_wt.py" line="198"/>
      <source>Wav files (*.wav)</source>
      <translation type="unfinished"/>
    </message>
  </context>
  <context>
    <name>EditDialog</name>
    <message>
      <location filename="../../mc/gui/rest_action_list_wt.py" line="215"/>
      <source>Title</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/breathing_phrase_list_wt.py" line="346"/>
      <source>Phrase(s)</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/rest_action_list_wt.py" line="284"/>
      <source>Please choose an image</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/rest_action_list_wt.py" line="284"/>
      <source>Image files</source>
      <translation type="unfinished"/>
    </message>
  </context>
  <context>
    <name>FeedbackDialog</name>
    <message>
      <location filename="../../mc/gui/feedback_dlg.py" line="51"/>
      <source>Show this dialog at startup again in the future</source>
      <translation type="unfinished"/>
    </message>
  </context>
  <context>
    <name>GeneralSettingsWt</name>
    <message>
      <location filename="../../mc/gui/settings_page_wt.py" line="45"/>
      <source>General Settings</source>
      <translation type="unfinished"/>
    </message>
  </context>
  <context>
    <name>MainWin</name>
    <message>
      <location filename="../../mc/gui/main_win.py" line="187"/>
      <source>Enable Rest Reminder</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/main_win.py" line="198"/>
      <source>Reset Rest Timer (Skip Break)</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/main_win.py" line="201"/>
      <source>Take a Break Now</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/main_win.py" line="207"/>
      <source>Enable Breathing Reminder</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/main_win.py" line="214"/>
      <source>Open Breathing Dialog</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/main_win.py" line="220"/>
      <source>Suspend Application</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/main_win.py" line="223"/>
      <source>Open Settings</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/main_win.py" line="446"/>
      <source>Quit</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/main_win.py" line="434"/>
      <source>&amp;File</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/main_win.py" line="435"/>
      <source>Export data</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/main_win.py" line="438"/>
      <source>Minimize to tray</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/main_win.py" line="451"/>
      <source>Suspend application</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/main_win.py" line="455"/>
      <source>&amp;Debug</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/main_win.py" line="456"/>
      <source>Update GUI</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/main_win.py" line="459"/>
      <source>Full screen</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/main_win.py" line="462"/>
      <source>Show rest reminder</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/main_win.py" line="465"/>
      <source>Show rest prepare</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/main_win.py" line="469"/>
      <source>&amp;Help</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/main_win.py" line="470"/>
      <source>Show intro wizard</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/main_win.py" line="473"/>
      <source>About</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/main_win.py" line="476"/>
      <source>Online help</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/main_win.py" line="479"/>
      <source>Give feedback</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/main_win.py" line="482"/>
      <source>System Information</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/main_win.py" line="719"/>
      <source>minute/s left until next rest</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/main_win.py" line="727"/>
      <source>Rest breaks disabled</source>
      <translation type="unfinished"/>
    </message>
  </context>
  <context>
    <name>RestActionListWt</name>
    <message>
      <location filename="../../mc/gui/rest_action_list_wt.py" line="28"/>
      <source>New item</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/rest_action_list_wt.py" line="37"/>
      <source>Add</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/rest_action_list_wt.py" line="47"/>
      <source>Edit the selected rest action</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/rest_action_list_wt.py" line="53"/>
      <source>Move the selected rest action to top</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/rest_action_list_wt.py" line="59"/>
      <source>Move the selected rest action up</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/rest_action_list_wt.py" line="65"/>
      <source>Move the selected rest action down</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/rest_action_list_wt.py" line="71"/>
      <source>Delete the selected rest action</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/rest_action_list_wt.py" line="85"/>
      <source>These are the actions that appear in the `rest dialog`</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/rest_action_list_wt.py" line="139"/>
      <source>You have to write an item before you press &apos;Add&apos;.</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/rest_action_list_wt.py" line="152"/>
      <source>Are you sure that you want to remove this entry?</source>
      <translation type="unfinished"/>
    </message>
  </context>
  <context>
    <name>RestDlg</name>
    <message>
      <location filename="../../mc/gui/rest_dlg.py" line="32"/>
      <source>Rest Actions</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/rest_dlg.py" line="38"/>
      <source>Please move and walk mindfully when leaving the computer</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/rest_dlg.py" line="50"/>
      <source>Close</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/rest_dlg.py" line="54"/>
      <source>Close and Breathe</source>
      <translation type="unfinished"/>
    </message>
  </context>
  <context>
    <name>RestPrepareDlg</name>
    <message>
      <location filename="../../mc/gui/rest_prepare.py" line="28"/>
      <source>Please prepare for rest</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/rest_prepare.py" line="32"/>
      <source>One minute left until the next rest</source>
      <translation type="unfinished"/>
    </message>
  </context>
  <context>
    <name>RestReminderDlg</name>
    <message>
      <location filename="../../mc/gui/rest_notification.py" line="36"/>
      <source>Please take good care of your body and mind</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/rest_notification.py" line="40"/>
      <source>Rest</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/rest_notification.py" line="51"/>
      <source>Wait</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/rest_notification.py" line="55"/>
      <source>Skip</source>
      <translation type="unfinished"/>
    </message>
  </context>
  <context>
    <name>RestSettingsWt</name>
    <message>
      <location filename="../../mc/gui/rest_settings_wt.py" line="23"/>
      <source>Visual + Audio</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/rest_settings_wt.py" line="24"/>
      <source>Visual</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/rest_settings_wt.py" line="80"/>
      <source>Audio</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/rest_settings_wt.py" line="27"/>
      <source>Select audio</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/rest_settings_wt.py" line="75"/>
      <source>Notifications</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/rest_settings_wt.py" line="77"/>
      <source>Notification type</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/rest_settings_wt.py" line="88"/>
      <source>Settings for Resting</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/rest_settings_wt.py" line="128"/>
      <source>Please choose a wav audio file</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/rest_settings_wt.py" line="128"/>
      <source>Wav files (*.wav)</source>
      <translation type="unfinished"/>
    </message>
  </context>
  <context>
    <name>RunOnStartupWt</name>
    <message>
      <location filename="../../mc/gui/general_settings_wt.py" line="13"/>
      <source>Run on startup</source>
      <translation type="unfinished"/>
    </message>
  </context>
  <context>
    <name>SettingsPageWt</name>
    <message>
      <location filename="../../mc/gui/settings_page_wt.py" line="22"/>
      <source>Breathing</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/settings_page_wt.py" line="23"/>
      <source>Resting</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/settings_page_wt.py" line="24"/>
      <source>Timers</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/settings_page_wt.py" line="26"/>
      <source>General settings</source>
      <translation type="unfinished"/>
    </message>
  </context>
  <context>
    <name>SuspendTimeDialog</name>
    <message>
      <location filename="../../mc/gui/suspend_time_dlg.py" line="53"/>
      <source>To resume after having suspended the application, drag the slider to the far left</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/suspend_time_dlg.py" line="78"/>
      <source>Application will resume in normal mode</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/suspend_time_dlg.py" line="82"/>
      <source>Application will be suspended for</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/suspend_time_dlg.py" line="82"/>
      <source>minutes</source>
      <translation type="unfinished"/>
    </message>
  </context>
  <context>
    <name>SysinfoDialog</name>
    <message>
      <location filename="../../mc/gui/sysinfo_dlg.py" line="40"/>
      <source>Copy to clipboard</source>
      <translation type="unfinished"/>
    </message>
  </context>
  <context>
    <name>TimingInitSetupPage</name>
    <message>
      <location filename="../../mc/gui/intro_dlg.py" line="277"/>
      <source>Rest after minutes</source>
      <translation type="unfinished"/>
    </message>
  </context>
  <context>
    <name>TimingSettingsWt</name>
    <message>
      <location filename="../../mc/gui/timing_settings_wt.py" line="46"/>
      <source>Reset the rest timer</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/timing_settings_wt.py" line="52"/>
      <source>Breathing Dialog</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/timing_settings_wt.py" line="61"/>
      <source>Interval every:</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/timing_settings_wt.py" line="63"/>
      <source>minutes</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/timing_settings_wt.py" line="59"/>
      <source>Rest Dialog</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/timing_settings_wt.py" line="64"/>
      <source>Time until next break:</source>
      <translation type="unfinished"/>
    </message>
    <message>
      <location filename="../../mc/gui/timing_settings_wt.py" line="75"/>
      <source>Settings for Timers</source>
      <translation type="unfinished"/>
    </message>
  </context>
  <context>
    <name>ToggleSwitchWt</name>
    <message>
      <location filename="../../mc/gui/toggle_switch_wt.py" line="17"/>
      <source>Turn the dialog and notifications on or off</source>
      <translation type="unfinished"/>
    </message>
  </context>
</TS>
